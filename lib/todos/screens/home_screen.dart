import 'package:flutter/material.dart';
import 'package:flutter_base_app/todos/screens/themes_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../todos/widgets/filtered_todos.dart';
import '../../todos/widgets/stats.dart';
import '../../todos/widgets/tab_selector.dart';
import '../../todos/widgets/filter_button.dart';
import '../../todos/widgets/extra_actions.dart';
import '../../todos/models/app_tab.dart';
import '../../todos/blocs/todos_blocs.dart';
import '../../auth/auth_bloc/auth.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TabBloc, AppTab>(builder: (context, activeTab) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Firestore Todos'),
          actions: <Widget>[
            FilterButton(visible: activeTab == AppTab.todos),
            IconButton(
              tooltip: 'Logout',
              icon: Icon(Icons.exit_to_app),
              onPressed: () =>
                  BlocProvider.of<AuthBloc>(context).add(LoggedOut()),
            ),
            IconButton(
              tooltip: 'Themes',
              icon: Icon(Icons.wb_sunny),
              onPressed: () =>
                  Navigator.of(context).pushNamed(ThemesScreen.namedRout),
            ),
            ExtraActions(),
          ],
        ),
        body: activeTab == AppTab.todos ? FilteredTodos() : Stats(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushNamed(context, '/addTodo');
          },
          child: Icon(Icons.add),
          tooltip: 'Add Todo',
        ),
        bottomNavigationBar: TabSelector(
          activeTab: activeTab,
          onTabSelected: (tab) =>
              BlocProvider.of<TabBloc>(context).add(UpdateTab(tab)),
        ),
      );
    });
  }
}
