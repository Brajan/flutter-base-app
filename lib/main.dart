import 'package:flutter/material.dart';
import 'package:flutter_base_app/theme/bloc/theme.dart';
import 'package:flutter_base_app/todos/screens/themes_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:todos_repository/todos_repository.dart';

import './splash_screen.dart';
import './core/simple_bloc_delegate.dart';
import './auth/repository/user_repository.dart';
import './auth/auth_bloc/auth.dart';
import './auth/screens/login_screen.dart';
import './todos/blocs/todos/todos.dart';
import './todos/blocs/todos_blocs.dart';
import './todos/screens/home_screen.dart';
import './todos/screens/add_edit_screen.dart';

void main() async {
  // Required in Flutter v1.9.4+ before using any plugins if the code is executed before runApp.
  WidgetsFlutterBinding.ensureInitialized();
//  BlocSupervisor.delegate = await HydratedBlocDelegate.build();
  BlocSupervisor.delegate =
      SimpleBlocDelegate(await HydratedBlocStorage.getInstance());
  final UserRepository userRepository = UserRepository();
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<ThemeBloc>(
          create: (context) => ThemeBloc(),
        ),
        BlocProvider<TodosBloc>(
          create: (context) {
            return TodosBloc(
              todosRepository: FirebaseTodosRepository(),
            )..add(LoadTodos());
          },
        ),
        BlocProvider<AuthBloc>(
          create: (context) => AuthBloc(
            userRepository: userRepository,
          )..add(AppStarted()),
        )
      ],
      child: BaseApp(userRepository: userRepository),
    ),
  );
}

class BaseApp extends StatelessWidget {
  final UserRepository _userRepository;

  const BaseApp({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeBloc, ThemeState>(builder: (context, state) {
      return MaterialApp(
        theme: state.themeData,
        routes: {
          '/': (context) {
            return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
              if (state is Unauthenticated) {
                return LoginScreen(userRepository: _userRepository);
              }
              if (state is Authenticated) {
                return MultiBlocProvider(
                  providers: [
                    BlocProvider<TabBloc>(
                      create: (context) => TabBloc(),
                    ),
                    BlocProvider<FilteredTodosBloc>(
                      create: (context) => FilteredTodosBloc(
                        todosBloc: BlocProvider.of<TodosBloc>(context),
                      ),
                    ),
                    BlocProvider<StatsBloc>(
                      create: (context) => StatsBloc(
                        todosBloc: BlocProvider.of<TodosBloc>(context),
                      ),
                    ),
                  ],
                  child: HomeScreen(),
                );
              }
              return SplashScreen();
            });
          },
          AddEditScreen.namedRout: (context) {
            return AddEditScreen(
              onSave: (task, note) {
                BlocProvider.of<TodosBloc>(context).add(
                  AddTodo(Todo(task, note: note)),
                );
              },
              isEditing: false,
            );
          },
          ThemesScreen.namedRout: (context) {
            return ThemesScreen();
          }
        },
      );
    });
  }
}
