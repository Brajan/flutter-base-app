import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_base_app/theme/app_themes.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import './theme.dart';

class ThemeBloc extends HydratedBloc<ThemeEvent, ThemeState> {
  @override
  ThemeState get initialState {
    return super.initialState ??
        ThemeState(themeData: appThemeData[AppTheme.BlueDark]);
  }

  @override
  Stream<ThemeState> mapEventToState(
    ThemeEvent event,
  ) async* {
    if (event is ThemeChanged) {
      yield ThemeState(themeData: appThemeData[event.theme]);
    }
  }

  @override
  ThemeState fromJson(Map<String, dynamic> json) {
    print(json);
    try {
      return ThemeState(themeData: _mapNameToTheme(json['ThemeData']));
    } catch (_) {
      return null;
    }
  }

  @override
  Map<String, dynamic> toJson(ThemeState state) {
    return {'ThemeData': _mapThemeToName(state.themeData)};
  }

  ThemeData _mapNameToTheme(String name) {
    switch (name) {
      case '1':
        return appThemeData[AppTheme.BlueDark];
      case '2':
        return appThemeData[AppTheme.BlueLight];
      case '3':
        return appThemeData[AppTheme.GreenLight];
      case '4':
        return appThemeData[AppTheme.GreenDark];
      default:
        return appThemeData[AppTheme.BlueDark];
    }
  }

  String _mapThemeToName(ThemeData themeData) {
    if (themeData == appThemeData[AppTheme.BlueDark]) {
      return '1';
    } else if (themeData == appThemeData[AppTheme.BlueLight]) {
      return '2';
    } else if (themeData == appThemeData[AppTheme.GreenLight]) {
      return '3';
    } else if (themeData == appThemeData[AppTheme.GreenDark]) {
      return '4';
    } else {
      return '1';
    }
  }
}
